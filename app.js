const express = require('express')
const app = express()
const cors = require('cors')
const webRouter = require("./server/routes/web");
const apiRouter = require('./server/routes')
const errorHandler = require('./server/middlewares/errorHandler')
const PORT = process.env.PORT || 4001
const path = require("path");
const expressLayout = require('express-ejs-layouts')

const swaggerUI = require("swagger-ui-express");
const swaggerJSON = require("./api-json.json");

// middlewares
app.use(cors())
app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(errorHandler)

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "/server/views"));
app.use(expressLayout)
app.set('layout', 'layouts/default')

/**
 * @Routes /api
 * entrypoint for all API routes
 */
 app.use(webRouter);
 app.use("/api", apiRouter);
 app.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerJSON));

 app.get('/', (req, res) => {
  res.send('<h1>Selamat Datang</h1>')
})

app.listen(PORT, () => {
  console.log(`Listening on http://localhost:${PORT}`)
})