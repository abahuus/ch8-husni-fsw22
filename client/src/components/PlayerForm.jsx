import React from 'react'
import { useState, useEffect } from 'react'
const PlayerForm = ({create, edit, cancelEdit, editing}) => {
    const [Email, setEmail] = useState('')
    const [Username, setUsername] = useState('')
    const [Password, setPassword] = useState('')
    const [Exp, setExp] = useState(0)

    useEffect(() => {
      if(typeof edit === 'object') {       
        setEmail(edit.email)
        setUsername(edit.username)
        setPassword(edit.password)
        setExp(edit.exp)
      }
    }, [edit])
    
    const submitHandler = (e) => {
        e.preventDefault()
        let player = {
            email: Email, 
            username: Username, 
            password: Password, 
            exp: Exp
        }
        if(typeof edit === 'object' && edit.id)
            editing({...player, id: edit.id})
        else
            create(player)

        reset()
    }

    const reset = () => {
        cancelEdit()
        setEmail('')
        setUsername('')
        setPassword('')
        setExp('')
    }
    
  return (    
    <div class="ml-2">
        <form onSubmit={e => submitHandler(e)}>
            <div className="mb-4">
                <label htmlFor="username" className="form-label">Username</label>
                <input onChange={(e) => setUsername(e.target.value)} type="text" className="form-control" id="username" value={Username} />
            </div>
            <div className="mb-4">
                <label htmlFor="email" className="form-label">Email address</label>
                <input type="email" onChange={(e) => setEmail(e.target.value)} className="form-control" id="email" value={Email} />
            </div>
            <div className="mb-4">
                <label htmlFor="password" className="form-label">Password</label>
                <input type="password" onChange={(e) => setPassword(e.target.value)} className="form-control" id="password" value={Password} />
            </div>
            <div className="mb-4">
                <label htmlFor="exp" className="form-label">Experience</label>
                <input type="number" onChange={(e) => setExp(e.target.value)} className="form-control" id="exp" value={Exp} />
            </div>
            <button type="submit" className="btn btn-primary me-2">Submit</button>
            <button type="button" onClick={reset} className="btn btn-danger">Reset</button>
        </form>
    </div>
  )
}

export default PlayerForm