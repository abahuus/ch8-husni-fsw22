import React from 'react'

const PlayerList = ({players, selectPlayer}) => {
  return (
<div>
    <table className="table align-middle mb-0 bg-white">
        <thead className="bg-light">
            <tr>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Experience</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            {players.map((player, index) => (
                <tr key={index}>
                    <td>{player.username}</td>
                    <td>{player.email}</td>
                    <td>{player.exp}</td>
                    <td><button onClick={() => selectPlayer(player.id)} type="button" className="btn btn-warning">Edit</button></td>
                </tr>
            ))}
        </tbody>
    </table>
</div>
  )
}

export default PlayerList