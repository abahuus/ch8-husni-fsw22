import 'bootstrap/dist/css/bootstrap.min.css';
import PlayerList from './components/PlayerList';
import PlayerForm from './components/PlayerForm';
import {useState} from 'react'
import React from 'react';
import "./App.css"

function App() {
  let defaultPlayer = [
    {
      id: 1,
      username: 'admin',
      email: 'admin@binar.com',
      password: 'admin',
      exp: 100
    },
    {
      id: 2,
      username: 'player1',
      email: 'player1@binar.com',
      password: 'player1',
      exp: 100
    },
    {
      id: 3,
      username: 'player2',
      email: 'player2@binar.com',
      password: 'player2',
      exp: 100
    },
    {
      id: 4,
      username: 'player3',
      email: 'player3@binar.com',
      password: 'player3',
      exp: 100
    },
    {
      id: 5,
      username: 'player4',
      email: 'player4@binar.com',
      password: 'player4',
      exp: 100
    },
    {
      id: 6,
      username: 'player5',
      email: 'player5@binar.com',
      password: 'player5',
      exp: 100
    },
  ]

  const [Players, setPlayers] = useState(defaultPlayer)
  const [edit, setEdit] = useState('')
  const [Keyword, setKeyword] = useState('')

  const addPlayer = (newPlayer) => {
    newPlayer.id = Math.max(...Players.map(player => player.id))+1
    setPlayers(Players => [...Players, newPlayer])
  }

  const selectPlayer = (playerId) => {
    let player = Players.filter(player => player.id === playerId)   
    setEdit(player[0])
  }

  const editing = (player) => {   
    const indexPlayer = Players.findIndex(item => item.id === player.id)  
    
    let copyPlayers = [...Players]
    copyPlayers[indexPlayer] = player

    setPlayers(copyPlayers)
  }

  const cancelEdit = () => {
    setEdit('')
  }

  const filteredData = () => {
    return Players.filter(player => new RegExp(Keyword, 'g').test(player.username) ||
    new RegExp(Keyword, 'g').test(player.email) ||
    new RegExp(Keyword, 'g').test(player.exp))
  }

  const bgImage = new URL("../public/logo512.png", import.meta.url)

  return (
    <div>
      <div className='picture d-flex align-items-center justify-content-between'>
        <img src={bgImage} alt=""/>
      </div>
      <div className="container mt-2">
        <div className="row">
          <div className="col-8 ">
            <div className="d-flex align-items-center justify-content-between">
              <h2>Players</h2>  
              <input type="text" className="form-control"  placeholder= "search....." onChange={(e) => setKeyword(e.target.value)} value={Keyword} />
            </div>
            <PlayerList players={filteredData()} selectPlayer={selectPlayer} />            
          </div>
          <div className="col-4 border p-3">
            <h2>Create/Edit Player</h2>
            <PlayerForm create={addPlayer} edit={edit} cancelEdit={cancelEdit} editing={editing} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
