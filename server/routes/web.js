const routes = require("express").Router();
const { Player } = require('../models')

routes.get("/dashboard", async (req, res) => {
  const data = await Player.findAll();
  res.render("dashboard", {data});
});

routes.get('/dashboard/player/:id', async (req, res) => {
  const {id} = req.params
  const data = await Player.findByPk(id);    
  res.render('player', {data})
})

routes.get("/dashboard/create", (req, res) => {
  res.render("create");
});

// routes.get("/dashboard/player", (req, res) => {
//   res.render("player");
// });

routes.get('/dashboard/:id/delete', async (req, res) => {
  const {id} = req.params
  const destroyed = await Player.destroy({
      where: { id: id },
  });
  if(destroyed)
      res.redirect('/dashboard')
})

module.exports = routes;
